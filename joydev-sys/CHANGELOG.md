## 0.2.1

- Make it so the project only builds on Linux (joydev isn't available elsewhere anyways)

## 0.2.0

- Add `extra_traits` feature to match libc behaviour
- `Eq`, `Hash` and `PartialEq` are now implemented when using `extra_traits`
- `Debug` is no longer implemented by default and is only available when using `extra_traits`

## 0.1.3

- Update documentation

## 0.1.2

- Update `Cargo.toml`

## 0.1.1

- Update `Cargo.toml`

## 0.1.0

- Initial release.