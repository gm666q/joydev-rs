[![Pipeline status](https://gitlab.com/gm666q/joydev-rs/badges/master/pipeline.svg)](https://gitlab.com/gm666q/joydev-rs/commits/master)
[![Coverage report](https://gitlab.com/gm666q/joydev-rs/badges/master/coverage.svg)](https://gitlab.com/gm666q/joydev-rs/commits/master)
[![Crates.io](https://img.shields.io/crates/v/joydev-sys)](https://crates.io/crates/joydev-sys)
[![Documentation](https://docs.rs/joydev-sys/badge.svg)](https://docs.rs/joydev-sys)
![License](https://img.shields.io/crates/l/joydev-sys)

# joydev-sys

This crate contains Linux joydev definitions from `linux/joystick.h`.

Those are raw definitions so for documentation see the official
[kernel documentation](https://www.kernel.org/doc/html/latest/input/joydev/joystick-api.html#joystick-api).